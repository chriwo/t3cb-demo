<?php

/** @var \TYPO3\Surf\Domain\Model\Deployment $deployment */
use TYPO3\Surf\Application\TYPO3\CMS;
use TYPO3\Surf\Domain\Model\Node;
use TYPO3\Surf\Domain\Model\SimpleWorkflow;

$projectName = getenv('PROJECT_NAME');
switch (getenv('DEPLOYMENT_CONTEXT')) {
    case 'Production':
        $maxReleases = getenv('DEPLOYMENT_KEEP_RELEASES') ?: 2;
        $remotePhpBinary = getenv('PRODUCTION_PHP_BINARY');
        $targetUrl = getenv('PRODUCTION_URI');
        $targetDeployPath = getenv('PRODUCTION_DEPLOYMENT_PATH');
        $targetDeployHost = getenv('PRODUCTION_DEPLOYMENT_HOST');
        $targetDeployUser = getenv('PRODUCTION_USER');
        break;
    default:
        $maxReleases = getenv('DEPLOYMENT_KEEP_RELEASES') ?: 5;
        $remotePhpBinary = getenv('STAGING_PHP_BINARY');
        $targetUrl = getenv('STAGING_URI');
        $targetDeployPath = getenv('STAGING_DEPLOYMENT_PATH');
        $targetDeployHost = getenv('STAGING_DEPLOYMENT_HOST');
        $targetDeployUser = getenv('STAGING_USER');
        break;
}

$appDirectory = 'public';//getenv('APP_DIRECTORY') ?: 'public';
$localComposerCommandPath = '/usr/bin/composer';
$localPhpBinary = '/usr/local/bin/php';
$repository = 'file://' . dirname(__DIR__);
$context = getenv('DEPLOYMENT_CONTEXT');

/**
 * Compare mode
 * "*" all updates
 * "field.*" all field updates
 * "*.add" add only new tables and fields
 * "*.change" only change updates
 * "*.add,*.change" all add and change updates
 *
 */
$databaseCompareMode = '*.add,*.change';

/*************************************************************************************************
 *
 * !!! Don't change the next lines or now what you do !!!
 *
 *************************************************************************************************/

$node = new Node($projectName);
$node
    ->setHostname($targetDeployHost)
    ->setOption('username', $targetDeployUser);

$application = new CMS();
$application
    ->addNode($node)
    ->setDeploymentPath($targetDeployPath)
    ->setOptions(
        array_merge(
            $application->getOptions(),
            [
                'context' => getenv('DEPLOYMENT_CONTEXT'),
                'initialDeployment' => false,
                'username' => $targetDeployUser,
                'phpBinaryPathAndFilename' => $remotePhpBinary,
                'keepReleases' => $maxReleases,
                'repositoryUrl' => $repository,
                'TYPO3\\Surf\\Task\\Package\\GitTask[hardClean]' => true,
                'composerCommandPath' => $localComposerCommandPath,
                'applicationRootDirectory' => $appDirectory,
                'publicDirectory' => $appDirectory,
                'webDirectory' => $appDirectory,
                'baseUrl' => 'http://testuser:test@' . $targetUrl,
                'databaseCompareMode' => $databaseCompareMode,
                'rsyncExcludes' => [
                    '.ddev',
                    '.reverse',
                    '.surf',
                    '/build/deployment',
                    '/craft',
                    '/frontend',
                    'node_modules',
                    '/public/fileadmin',
                    '/public/uploads',
                    '.DS_Store',
                    '.babelrc',
                    '.editorconfig',
                    '.eslintignore',
                    '.eslintrc.json',
                    '.git',
                    '.gitattributes',
                    '.gitignore',
                    '.gitkeep',
                    '.gitlab-ci.yml',
                    '.php_cs.dist',
                    '.stylelintignore',
                    '.stylelintrc',
                    'composer.json',
                    'composer.lock',
                    'LICENSE',
                    'package.json',
                    'package-lock.json',
                    'Readme.md',
                    'webpack.config.js'
                ]
            ]
        )
    );

setOptionDeploymentSource($application);
$deployment->addApplication($application);

$workflow = new SimpleWorkflow();

$deploymentTasks = explode(',', getenv('DEPLOYMENT_TASKS'));
if (!empty($deploymentTasks)) {
    foreach ($deploymentTasks as $task) {
        if (function_exists((string)$task)) {
            call_user_func_array(
                (string)$task,
                [$context, $remotePhpBinary, &$workflow]
            );
        }
    }
}

$workflow->removeTask('TYPO3\\Surf\\Task\\TYPO3\\CMS\\CopyConfigurationTask');
$workflow->addTask('TYPO3\\Surf\\Task\\TYPO3\\CMS\\CompareDatabaseTask', 'migrate');

$deployment->setWorkflow($workflow);

/**
 * Fix access rights for files and folders
 *
 * @param string $context
 * @param string $remotePhpBinary
 * @param \TYPO3\Surf\Domain\Model\SimpleWorkflow $workflow
 */
function fixAccessRightsTask($context, $remotePhpBinary, &$workflow)
{
    $workflow
        ->defineTask(
            __FUNCTION__,
            'TYPO3\\Surf\\Task\\ShellTask',
            [
                'command' =>
                    'cd {releasePath} && ' .
                    'find -type d -print0 | xargs -0 chmod 2775 && ' .
                    'find -type f -print0 | xargs -0 chmod 0664 && ' .
                    'cd ./build/cronjobs && ' .
                    'find -type f -print0 | xargs -0 chmod 0750',
                'logOutput' => true,
                'ignoreErrors' => true
            ]
        )
        ->afterStage('transfer', __FUNCTION__);
}

/**
 * Fix folder structure with TYPO3 console
 *
 * @param string $context
 * @param string $remotePhpBinary
 * @param \TYPO3\Surf\Domain\Model\SimpleWorkflow $workflow
 */
function fixFolderStructureTask($context, $remotePhpBinary, &$workflow)
{
    $workflow
        ->defineTask(
            __FUNCTION__,
            'TYPO3\\Surf\\Task\\ShellTask',
            [
                'command' =>
                    $remotePhpBinary . ' {releasePath}/vendor/bin/typo3cms install:fixfolderstructure',
                'logOutput' => true
            ]
        )
        ->afterStage('finalize', __FUNCTION__);
}

/**
 * Update languages with TYPO3 console
 *
 * @param string $context
 * @param string $remotePhpBinary
 * @param \TYPO3\Surf\Domain\Model\SimpleWorkflow $workflow
 */
function updateLanguageTask($context, $remotePhpBinary, &$workflow)
{
    $workflow
        ->defineTask(
            __FUNCTION__,
            'TYPO3\\Surf\\Task\\ShellTask',
            [
                'command' =>
                    $remotePhpBinary . ' {releasePath}/vendor/bin/typo3cms language:update',
                'logOutput' => true,
                'ignoreErrors' => true
            ]
        )
        ->afterStage('finalize', __FUNCTION__);
}

/**
 * Remove index.php symlink from web root and copy index.php from TYPO3-Core into web root
 * This task is required for hosting partner "DomainFactory" and "jWeiland"
 *
 * @param string $context
 * @param string $remotePhpBinary
 * @param \TYPO3\Surf\Domain\Model\SimpleWorkflow $workflow
 */
function copyIndexFileTask($context, $remotePhpBinary, &$workflow)
{
    $file = 'index.php';
    $target = '{workspacePath}/public/' . $file;
    $copySource = '{workspacePath}/vendor/typo3/cms/' . $file;

    $command =
        'rm -f ' . $target . ' && ' .
        'cp -f ' . $copySource . ' ' . $target;

    $workflow
        ->defineTask(
            __FUNCTION__,
            'TYPO3\\Surf\\Task\\LocalShellTask',
            [
                'command' => $command,
                'logOutput' => true,
                'ignoreErrors' => false
            ]
        )
        ->afterStage('package', __FUNCTION__);
}

/**
 * Copy context relevant files for instances (htaccess). In non-production mode add also
 * a htpasswd file to protect this instances.
 *
 * @param string $context
 * @param string $remotePhpBinary
 * @param \TYPO3\Surf\Domain\Model\SimpleWorkflow $workflow
 */
function copyContextFileTask($context, $remotePhpBinary, &$workflow)
{
    $sourceDirectory = getenv('CI_PROJECT_DIR') . '/build/deployment/' . getContextDirectoryName($context);
    $targetDirectory = '{workspacePath}/public';

    $command =
        'rm -f ' . $targetDirectory . '/.htaccess && ' .
        'cp -f ' . $sourceDirectory . '/.htaccess ' . $targetDirectory . '/.htaccess';

    if ('production' !== strtolower($context)) {
        $command .= ' && ' .
            'rm -f ' . $targetDirectory . '/.htpasswd && ' .
            'cp -f ' . $sourceDirectory . '/.htpasswd ' . $targetDirectory . '/.htpasswd';
    }

    $workflow
        ->defineTask(
            __FUNCTION__,
            'TYPO3\\Surf\\Task\\LocalShellTask',
            [
                'command' => $command,
                'logOutput' => true,
                'ignoreErrors' => false
            ]
        )
        ->afterStage('package', __FUNCTION__);
}

/**
 * Execute an prepare database sql file
 *
 * @param string $context
 * @param string $remotePhpBinary
 * @param \TYPO3\Surf\Domain\Model\SimpleWorkflow $workflow
 */
function prepareDatabaseTask($context, $remotePhpBinary, &$workflow)
{
    $releaseDeploymentPath = '{releasePath}/build/' . getContextDirectoryName($context) . '/';

    $workflow
        ->defineTask(
            __FUNCTION__,
            'TYPO3\\Surf\\Task\\ShellTask',
            [
                'command' =>
                    $remotePhpBinary . ' {currentPath}/vendor/bin/typo3cms database:import < ' .
                    $releaseDeploymentPath . 'db-prepare.sql',
                'logOutput' => true,
                'ignoreErrors' => false
            ]
        )
        ->afterStage('migrate', __FUNCTION__);
}

/**
 * Install the app frontend with yarn and publicpack.
 *
 * @param string $context
 * @param string $remotePhpBinary
 * @param \TYPO3\Surf\Domain\Model\SimpleWorkflow $workflow
 */
function frontendInstallTask($context, $remotePhpBinary, &$workflow)
{
    $command =
        'cd {workspacePath} && npm install && yarn build-prod';
    $workflow
        ->defineTask(
            __FUNCTION__,
            'TYPO3\\Surf\\Task\\LocalShellTask',
            [
                'command' => $command,
                'logOutput' => true,
                'ignoreErrors' => false
            ]
        )
        ->beforeStage('transfer', __FUNCTION__);
}

/**
 *
 * @param $context
 * @return string|mixed
 */
function getContextDirectoryName($context)
{
    return str_replace('/', '-', strtolower($context));
}

/**
 * This method checks whether there is a correct deployment source specified. If not, it throws an exception
 * TODO: This method is not project specific and
 * may be put into something like a Library of Surf deployment related
 * classes in the future.
 *
 * @param  \TYPO3\Surf\Application\TYPO3\CMS $application
 * @throws \Exception
 * @return void
 */
function setOptionDeploymentSource(CMS &$application)
{
    $source = getenv('DEPLOYMENT_SOURCE');

    if (!is_string($source)) {
        throw new \Exception(
            'DEPLOYMENT_SOURCE environment variable is missing. Pattern: "DEPLOYMENT_SOURCE=branch|tag|sha1:foobar"',
            1479391741322
        );
    }

    $sourceArray = explode(':', $source);

    if (count($sourceArray) === 2
        && in_array($sourceArray[0], ['sha1', 'branch', 'tag'])
    ) {
        $application->setOption($sourceArray[0], $sourceArray[1]);
    } else {
        throw new \InvalidArgumentException(
            'DEPLOYMENT_SOURCE environment variable does not meet the mandatory pattern. Pattern: "DEPLOYMENT_SOURCE=branch|tag|sha1:foobar", 1479391747337',
            1455797642
        );
    }
}

# Konfiguration Gitlab-CI mit Surf

Das Gitlab-CI übernimmt die Aufgabe, Projekte aus einem Git-Repository auf einen Zielserver (Production oder Staging)
auszuliefern und eine Überprüfung der Code-Qualität durchzuführen.

Durch diesen Mechanismus kann zum einem die Entwicklung eines Projektes lokal erfolgen und zum anderen ist es nicht mehr
notwendig Dateien per FTP auf einen Zielserver zu übertragen, wodurch die Fehlerqote beim ausliefern des Projektes
minimiert wird.

Die Auslieferung des Projektes erfolgt in mehreren Schritten (Tasks), wobei alle Schritte fehlerfrei durchlaufen werden
müssen. Sollte einer dieser Schritte fehlschlagen, wird ein *Rollback* durchgeführt, wodurch der vorherige Zustand des
Projektes erhalten und online bleibt. 

änderung


## Vorbereitungen

### .env Datei erstellen und konfigurieren

Auf dem Zielserver muss in das Verzeichnis */shared/conf/* die Datei *.env* erstellt werden. Eine Beispieldatei
befindet sich innerhalb des Git-Projektes unter */build/deployment/.env_example*.

> Diese **.env** enthält wichtige Parameter für das TYPO3-System auf dem Zielserver und muss manuell angelegt werden.

* **TYPO3_CONTEXT** kann den Wert **Production** oder **Production/Staging** annehmen und muss entsprechend dem
Zielserver definiert werden.
* **TYPO3_INSTALL_TOOL_PASSWORD** muss im Klartext eingetragen werden

Sollten sich Sonderzeichen in den Werten befinden, z.B. bei Passwörtern, müssen die mit doppelten Hochkommata (")
umschlossen sein.

> Beispiel: TYPO3_DB_DEFAULT_PASSWORD="7$6334,%#3"

Werte die nicht gesetzt sind, werden ignoriert.



### Private-Public Keys

ToDo



### Environment-Variablen bei Gitlab

Die Gitlab Environments sind Variablen, welche wir mit Werten befüllen, welche wiederum während des Deployment abgefragt
werden. Durch diese Environments müssen keine sensiblen Daten innerhalb des Projektes gespeichert werden.

#### Wo anlegen?

In dem Git-Repository bei Gitlab links in der Navigation auf **Settings >> CI/CD** und dann den Punkt **Variable**
aufklappen. Dort werden nun die Variablen erstellt und ihnen entsprechende Werte zugewiesen.

#### Welche Variablen?

##### DEPLOYMENT_PRIVATE_KEY

Inhalt der Datei **id_rsa** welche im Schritte "Private-Public Keys" erstellt wurde

##### PRODUCTION_DEPLOYMENT_HOST

Entweder eine IP-Adresse oder die URL **ohne** "http(s)" und "www"

##### PRODUCTION_DEPLOYMENT_PATH

Der absolute Pfad wohin deployed werden soll. Dieser Pfad kann einfach auf der Kommandozeile in Erfahrung gebracht
werden. Dazu mit dem Zielserver per SSH verbinden und dann in das entsprechende Verzeichnis navigieren. Dort angekommen,
gibt man den Befehle `pwd` ein und erhält den absoluten Pfad.

Beispiel Mittwald: `/home/www/p-NUMMER/html/typo3`

##### PRODUCTION_PHP_BINARY

Die Angabe der richtigen PHP-Version ist zwingend notwendig. Hat man eine SSH-Verbindung mit dem Zielserver, kann die
Version mittels `php --version` geprüft werden und mit `which php` den Pfad ermitteln.

Für folgende Hoster gelten diese Pfade:

* Mittwald: `php_cli` (PHP Version der Domain)
* Domain Factory: `/usr/local/bin/php7-70STABLE-CLI`, `/usr/local/bin/php7-71STABLE-CLI`, `/usr/local/bin/php7-72STABLE-CLI`
* jWeiland: `/usr/local/bin/php7-70STABLE-CLI`, `/usr/local/bin/php7-71STABLE-CLI`, `/usr/local/bin/php7-72STABLE-CLI`
* All-Inkl: `php` (PHP Version der Domain)
* Metanet: `/opt/php70/bin/php`, `/opt/php71/bin/php`, `/opt/php72/bin/php`

##### PRODUCTION_URI

Hier muss die URL des Zielsystems eingetragen, **ohne** http(s) und www.

##### PRODUCTION_USER

Der SSH User mit welchen die SSH Verbindung zum Zielserver hergestellt werden kann.

* Mittwald: Account-User
* DomainFactory: kann selbst definiert werden
 
##### PROJECT_NAME

Der Name des Projektes, zumeist wird hier der Kundenname verwendet.

##### STAGING_*

Für den Staging-Server müssen die gleichen Variablen wie für den Production-Server vorhanden sein, jedoch mit entsprechend
angepassten Werten.





## Deployment Jobs

In der **.gitlab-ci.yml** sind 2 Stages definiert, *build* und *deploy*, welche unterschiedliche Aufgaben haben.



### Job: build

> Wird automatisch gestartet!

Der build-Stage wird genutzt um eine Prüfung des geschriebenen Sass und JavaScript Codes durchzuführen. Dazu werden
die Linter *eslint* (JavaScript) und *stylelint* (CSS/Sass) genutzt. Anhand von definierten Regeln prüfen diese Linter
ob die Schreibweisen und Einrückungen des Codes korrekt sind.

> Gibt es in diesem Stage keine Fehler, kann der Job *deploy_to_staging* oder *deploy_to_production* ausgeführt werden!

Dieser Stage wird nur dann ausgeführt und ist in der Gitlab-CI GUI sichtbar, wenn es Änderungen in den Verzechnissen
*/frontend/assets/JavaScript/* und/oder */frontend/assets/Sass/* gibt.



### Job: deploy_to_staging

> Muss manuell gestartet werden!

Dieser Job überträgt die Applikation (Website) aus dem Git-Repository auf das Zielsystem "Staging". "Staging" ist das
System auf welchem der Kunde die Abnahme des Projektes vor nimmt. 



##### Default Einstellungen

| Einstellung | Wert               |
| :-------    | :----------------- |
| Ausführung  | Manuell            |
| Context     | Production/Staging |
| Source      | Commit             |
| Releases    | 2                  |
| Tasks       | fixAccessRightsTask<br>fixFolderStructureTask<br>copyContextFileTask<br>frontendInstallTask |



### Job: deploy_to_production

Mit diesem Job wird die Applikation (Website) auf das produktive Zielsystem übertragen und die Applikation ist live.

##### Default Einstellungen

| Einstellung | Wert               |
| :-------    | :----------------- |
| Ausführung  | Manuell            |
| Context     | Production         |
| Source      | Commit             |
| Releases    | 5                  |
| Tasks       | fixAccessRightsTask<br>fixFolderStructureTask<br>copyContextFileTask<br>frontendInstallTask |


## Tasks

Für das Deployment zum jeweiligen Zielserver (Production oder Staging) stehe verschiedene Task zur Verfügung, welche
je nach Zielserver genutzt werden können.

Die Tasks werden in der Datei **.gitlab-ci.yml** in der Variable *DEPLOYMENT_TASKS* kommata separiert definiert. Die
Reihenfolge der Definition ist nicht relevant, da dies bereits innerhalb der Surf-Deployment-Beschreibung festgelegt
ist.

##### Task: copyContextFileTask

> Status: Default aktiv

Dieser Task kopiert die Datei **.htaccess** aus dem Verzeichnis */build/deployment/ZIEL-STAGE/* in das Web-Root
Verzeichnis. Daher ist es möglich je nach Zielserver unterschiedliche Konfigurationen für die **.htaccess** zu nutzen.

Sofern der ZIEL-STAGE nicht das Production-System ist, wird ebenfalls, falls vorhanden die Datei **.htpasswd** in das
Web-Root Verzeichnis kopiert.

##### Task: copyIndexFileTask

> Status: Default inaktiv

Bei einigen Hostingpartner wird der Symlink auf die **index.php** nicht erlaubt (z.B. DomainFactory, jWeiland). Daher
löscht dieser Task diesen Symlink und kopiert die **index.php** Datei aus dem TYPO3-Core in den Web-Root.

##### Task: fixAccessRightsTask

> Status: Default aktiv

Passt die Datei- und Ordnerzugriffsrechte während des Deployments an. Damit wird gewährleistet das auf alle notwendigen
Dateien und Ordner gelesen und geschrieben werden kann (z.B. typo3temp Ordner).

##### Task: fixFolderStructureTask

> Status: Default aktiv

In den Git-Repositories sind nicht alle Verzeichnisse enthalten welche TYPO3 benötigt, daher erstellt dieser Task alle
notwendigen Ordner.

##### Task: frontendInstallTask

> Status: Default aktiv

Dieser Task kompiliert die CSS und JavaScript Dateien, sowie die Icons und Grafiken die für das Layout notwendig sind und
legt dieser Dateien innerhalb der Extension *customer_sitepackage* ab.  

##### Task: prepareDatabaseTask

> Status: Default inaktiv

In dem Verzeichnis */build/deployment/ZIEL-STAGE/* kann eine Datei **db-prepare.sql** abgelegt werden, in welcher
bestimmte SQL-Befehle auf der Datenbank abgesetzt werden können. 

##### Task: updateLanguageTask

> Status: Default inaktiv

Mit diesem Task werden die Sprachpakete für TYPO3-Core, als auch für alle installierten Extension während des Deployments
aktualisiert.

Dieser Task kann u.U. fehlschlagen, wenn der Translation-Server von TYPO3 nicht erreichbar ist und somit würde entweder
das Deployment abbrechen oder es steht nur die Default-Sprache (englisch) zur Verfügung.

Daher hat es sich als bewährt gezeigt die Aktualisierung der Sprachpakete lokal auszuführen und die Änderungen mit im
Git-Repository zu speichern.

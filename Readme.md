# TYPO3 Projekt - Customer

## 1. Domain Übersicht

| System      | URL                                    |
| ----------  | ---------------------------------------|
| Production  |          |
| Staging     |          |
| Development | http://###DDEV_URL###         |

## 1. Verwendung

### 1.1 Systemvoraussetzungen für lokale Entwicklung

* Windows 10 Pro, macOs Sierra oder höher, Linux
* [Docker][1] version 17.05 oder höher
* [docker-compose][2] 1.10.0 oder höher (ist als Bundle für "Docker for Mac" bzw. "Docker für Windows" enthalten)
* [ddev][3] version 1.6.0 oder höher
* [Git][4]

### 1.2 Installation

* Download des Projektes mittels `git clone REPO-URL <project-name>`
* Auf der Konsole (Shell) in das Verzeichnis _<project-name>_ wechseln (z.B. `cd ~/Projekte/website_EXAMPLE`)
* Auf der Konsole (Shell) die ddev Konfiguration auf die aktuelle Version anpassen mit `ddev config`
* Projekt initialisieren bzw. starten mit `ddev start`

Beim ersten Start des Projektes nimmt dies ein wenig Zeit in Anspruch ca. 2-3 Minuten bis alle Resourcen installiert und
die Daten vom remote Server geladen wurden (Ordner fileadmin, Ordner uplodas und ein Dump der Datenbank).

### 1.3 Projekt starten und stopen

Um das Projekt zu starten und zu stoppen sind auf der Shell nur 2 Befehle notwendig

* `ddev start` - Startet das Projekt und damit alle notwendigen Docker-Container (Webserver, Datenbank, MailClient)
* `ddev stop` - hält das Projekt an und fährt alle Docker-Container herunter

> **Das stoppen des Projektes ist sehr wichtig! Teilweise kommt es vor, dass der DB-Server nicht korrekt herunter fährt
und somit eure DB-Änderungen ggf. verloren gehen**

### 1.4 Projekt im Browser aufrufen

Sofern die lokalen URLs für das Projekt bekannt sind, kann dieser Schritt übersprungen werden. Falls nicht, hilft auf der 
Shell `ddev describe` und es werden alle Frontend-Urls aufgelistet.

* http://###DDEV_URL###

Um Zugriff auf die DB oder den Mail-Client zu erhalten, können folgende URLs verwendet werden.

MailHog:   	http://###DDEV_URL###:8025
phpMyAdmin:	http://###DDEV_URL###:8036

MailHog fängt für euch *alle* E-Mails ab, die über das System versendet werden. Somit kann keine E-Mail versehentlich 
an einen Kunden verschickt werden und ihr könnt die E-Mail Ausgabe prüfen, z.b bei Newsletter interessant.

### 1.5 Projektdaten aktualisieren

Um Projektdaten wie Datenbank oder Dateien (z.B. aus dem fileadmin oder uploads Ordner) zu aktualisieren, muss man sich 
zunächst mit dem Docker-Web-Container verbinden `ddev ssh` und welchselt anschließen mit `cd ..` einen Ordner höher.
Nun gibt man einen der folgenden Befehle ein

* `vendor/bin/typo3reverse reverse_db`- lädt die aktuelle DB herunter
* `vendor/bin/typo3reverse reverse_files` - lädt die neusten und geänderten Dateien aus fileadmin und uploads Ordner herunter
* `vendor/bin/typo3reverse reverse_full` - lädt DB und neuste/geänderte Dateien aus dem fileadmin und uploads Ordner herunter

> Bitte beachten, das keine Frontend- und Backend-Benutzer heruntergeladen werden (DGSVO)!

#### 1.5.1 Backend login

Im TYPO3 Backend kann sich immer wie folgt eingeloggt werden:

Benutzername: admin
Passwort: password

InstallTool: joh316

### 1.6. Projekt löschen

Habt ihr auf eurem Rechner keinen Speicherplatz mehr und benötigt das Projekt nicht, könnt ihr dieses vollständig löschen.
Dazu gebt in der Shell `ddev remove --remove-data --omit-snapshot` ein. Damit wird die Datenbank vernichtet. Anschließen
kann das Projekt völlständig über den "Windows Explorer", den "Finder" unter Mac oder per Shell gelöscht werden.

Um später wieder am gleichen Projekt zu arbeiten fangt einfach bei Schritt _1.2 Installation_ an.

## 3. Deployment

Durch das integrierte Surf-Deployment mittels Gitlab-CI, kann der aktuelle Stand auf eine Staging- und einen
Production-Server ausgeliefert werden. Wir das Deployment bei Gitlab eingerichtet werden muss kann in der
[Dokumentation][5] nachgelesen werden.

## 2. Für Administratoren

### 2.1. Versionen and support

| Website     | TYPO3      | PHP        | Support/Development                     |
| ----------- | ---------- | ---------- |---------------------------------------- |
| 1.x         | 9.5        | ###DDEV_PHP_VERSION###        | Features, Bugfixes, Security Updates    |

### 2.2 Verwendete TYPO3 Extensions

| Extension        | Version        |
| --------------   | -----------    |
| Starter          | dev-task-compatibility-t3v9     |
| TYPO3-Console    | ^5.5.5 |


[1]: https://www.docker.com/community-edition
[2]: https://docs.docker.com/compose/install/
[3]: https://ddev.readthedocs.io/en/latest/#installation
[4]: https://git-scm.com/downloads
[5]: /.surf/Readme.md

<?php

$GLOBALS['TYPO3_CONF_VARS'] = array_replace_recursive(
    $GLOBALS['TYPO3_CONF_VARS'],
    [
        'BE' => [
            'debug' => true,
            'languageDebug' => false,
            'sessionTimeout' => 60 * 60 * 24 * 365
        ],
        'FE' => [
            'debug' => true
        ],
        'SYS' => [
            'displayErrors' => true,
            'devIPmask' => '*',
            'sqlDebug' => true,
            'enableDeprecationLog' => 'file',
            'systemLogLevel' => 0,
            'trustedHostsPattern' => '.*',
            'systemLog' => 'error_log',
            'syslogErrorReporting' => true,
            'belogErrorReporting' => true
        ]
    ]
);

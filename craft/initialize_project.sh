#!/bin/bash

PROJECT_DIRECTORY="/var/www/html"

pushd "/var/www/html"

DATABASE_BACKUP_PATH="$PROJECT_DIRECTORY/craft/data"

# install dependencies
composer install --prefer-dist

# add local development configuration
rm -f public/.htaccess && cp build/deployment/development/.htaccess public/.htaccess

vendor/bin/typo3cms install:fixfolderstructure
vendor/bin/typo3cms install:generatepackagestates

# Wait for db to become available
until mysqladmin ping -h db > /dev/null 2>&1
do
    echo "Waiting 5sec for db to become available";
    sleep 5;
done

vendor/bin/typo3cms database:updateschema "*.add,*.change"
vendor/bin/typo3cms extension:setupactive

echo "Import database dumps"
cat ${DATABASE_BACKUP_PATH}/*.sql | vendor/bin/typo3cms database:import
cat build/deployment/development/*.sql | vendor/bin/typo3cms database:import

# add or change database after dump-import
vendor/bin/typo3cms database:updateschema "*.add,*.change"

# Remove all BE user to have no personal data (DSGVO)
echo "TRUNCATE TABLE be_users" | vendor/bin/typo3cms database:import

# Create be admin
vendor/bin/typo3cms backend:createadmin admin password

# Finished cache
vendor/bin/typo3cms cache:flush

popd

<?php
defined('TYPO3_MODE') || die();

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile(
    'customer_sitepackage',
    'Configuration/TypoScript',
    'Customer'
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile(
    'customer_sitepackage',
    'Configuration/TypoScript/Context/Staging',
    'Staging Customer [optional]'
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile(
    'customer_sitepackage',
    'Configuration/TypoScript/Context/Development',
    'Development Customer [optional]'
);

<?php

(function () {
    // Load .env file in shared/conf
    if (!\TYPO3\CMS\Core\Utility\GeneralUtility::getApplicationContext()->isDevelopment()) {
        $dotEnvFile = __DIR__ . '/../../../../shared/conf/.env';
        if (file_exists($dotEnvFile)) {
            $loader = new Symfony\Component\Dotenv\Dotenv();
            $loader->load($dotEnvFile);
        } else {
            die('Please provide a .env file.');
        }
    }

    $GLOBALS['TYPO3_CONF_VARS'] = array_replace_recursive(
        $GLOBALS['TYPO3_CONF_VARS'],
        [
            'DB' => [
                'Connections' => [
                    'Default' => [
                        'dbname' => getenv('TYPO3_DB_DEFAULT_DBNAME'),
                        'host' => getenv('TYPO3_DB_DEFAULT_HOST'),
                        'password' => getenv('TYPO3_DB_DEFAULT_PASSWORD'),
                        'port' => getenv('TYPO3_DB_DEFAULT_PORT'),
                        'user' => getenv('TYPO3_DB_DEFAULT_USER')
                    ]
                ]
            ],
            'LOG' => [
                'writerConfiguration' => [
                    \TYPO3\CMS\Core\Log\LogLevel::WARNING => [
                        \TYPO3\CMS\Core\Log\Writer\FileWriter::class => [
                            'logFile' => dirname(PATH_site) . '/build/log/typo3-default.log'
                        ]
                    ]
                ]
            ]
        ]
    );

    if (getenv('CONTEXT')) {
        $GLOBALS['TYPO3_CONF_VARS']['SYS']['sitename'] .= ' [' . getenv('CONTEXT') . ']';
    }

    // mail
    if (! empty(getenv('TYPO3_BE_WARNING_EMAIL_ADDR'))) {
        $GLOBALS['TYPO3_CONF_VARS']['BE']['warning_email_addr'] = getenv('TYPO3_BE_WARNING_EMAIL_ADDR');
        $GLOBALS['TYPO3_CONF_VARS']['BE']['warning_mode'] = getenv('TYPO3_BE_WARNING_MODE');
    }

    if (! empty(getenv('TYPO3_MAIL_DEFAULT_MAIL_FROM_ADDRESS'))) {
        $GLOBALS['TYPO3_CONF_VARS']['MAIL']['defaultMailFromAddress'] = getenv('TYPO3_MAIL_DEFAULT_MAIL_FROM_ADDRESS');
        $GLOBALS['TYPO3_CONF_VARS']['MAIL']['defaultMailFromName'] = getenv('TYPO3_MAIL_DEFAULT_MAIL_FROM_Name');
    }

    // Graphics processor path
    if (!empty(getenv('TYPO3_GFX_PROCESSOR'))) {
        $GLOBALS['TYPO3_CONF_VARS']['GFX']['processor'] = getenv('TYPO3_GFX_PROCESSOR');
    }
    if (!empty(getenv('TYPO3_GFX_PROCESSOR_PATH'))) {
        $GLOBALS['TYPO3_CONF_VARS']['GFX']['processor_path'] = getenv('TYPO3_GFX_PROCESSOR_PATH');
    }
    if (!empty(getenv('TYPO3_GFX_PROCESSOR_PATH_LZW'))) {
        $GLOBALS['TYPO3_CONF_VARS']['GFX']['processor_path_lzw'] = getenv('TYPO3_GFX_PROCESSOR_PATH_LZW');
    }

    // install tool
    if (getenv('TYPO3_INSTALL_TOOL_PASSWORD')) {
        /** @var \TYPO3\CMS\Saltedpasswords\Salt\SaltFactory $saltFactory */
        $saltFactory = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Saltedpasswords\Salt\SaltFactory::class);
        $hashInstance = $saltFactory->getDefaultHashInstance('BE');
        $plaintextPassword = getenv('TYPO3_INSTALL_TOOL_PASSWORD');
        $saltedPassword = $hashInstance->getHashedPassword($plaintextPassword);

        $GLOBALS['TYPO3_CONF_VARS']['BE']['installToolPassword'] = $saltedPassword;
    }

    // Development configuration
    if (\TYPO3\CMS\Core\Utility\GeneralUtility::getApplicationContext()->isDevelopment()) {
        $files = glob(PATH_site . '../build/deployment/development/configuration/*.php');
        foreach ($files as $configurationFile) {
            require_once($configurationFile);
        }
    }
})();

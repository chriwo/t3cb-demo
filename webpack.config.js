const path = require('path')
const webpack = require('webpack')
const CopyPlugin = require('copy-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')

const projectOutputPath = 'packages/customer_sitepackage/Resources/Public/'

const config = {
  entry: ['./frontend/assets/JavaScript/app.js'],
  output: {
    path: path.resolve(__dirname, projectOutputPath),
    filename: 'JavaScript/app.js'
  },
  plugins: [
    new CopyPlugin([
      {
        from: 'node_modules/jquery/dist/jquery.min.js',
        to: 'JavaScript/jquery.min.js'
      },
      { from: 'frontend/assets/Images/', to: 'Images/' }
    ]),
    new MiniCssExtractPlugin({
      filename: 'app.css'
    }),
    new webpack.ProvidePlugin({
      $: 'jquery',
      jQuery: 'jquery'
    })
  ],
  module: {
    rules: [
      {
        test: /\.m?js$/,
        exclude: /(node_modules)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env']
          }
        }
      },
      {
        test: /\.(woff|woff2|eot|ttf)$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[name].[ext]',
              outputPath: 'Fonts/'
            }
          }
        ]
      },
      {
        test: /\.(jpg|jpeg|png|svg)$/,
        use: [
          'url-loader?limit=100000'
        ]
      },
      {
        test: /\.scss$/,
        use: [
          'style-loader',
          MiniCssExtractPlugin.loader,
          'css-loader',
          'sass-loader'
        ]
      }
    ]
  }
}

module.exports = config
